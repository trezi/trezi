package com.trezi.game.trezi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TreziApplication {

	public static void main(String[] args) {
		SpringApplication.run(TreziApplication.class, args);
	}
}
